﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UMOV.POCO;
using UMOV;
using UMOV.BLL;
using HipFat;
using HipFat.DAL;
using HipFat.BLL;
using HipFat.POCO;
using System.IO;
using System.Reflection;
using static System.Net.Mime.MediaTypeNames;
using System.Timers;

namespace HipcomApiUmov.Controllers
{
    public class HomeController : Controller
    {
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            UMOV.BLL.ScheduleBLL scheduleBLL = new ScheduleBLL();

            string path = LogWriter.LogPath;
            ViewBag.Title = "Home Page";
            ViewBag.Domain = ConfigurationManager.AppSettings["domain"];
            ViewBag.Url = ConfigurationManager.AppSettings["umovURL"];
            ViewBag.Domain = ConfigurationManager.AppSettings["domain"];
            ViewBag.SyncTimer = ConfigurationManager.AppSettings["syncTimer"];
            ViewBag.LogDirectory = path;

            string logText;

            //PrimeiraIntegracao();
            scheduleBLL.VerificarEIntegrarNovosChamados();
            scheduleBLL.VerificarEAtualizarVisitas();

            try
            {
                string pattern = "*.txt";
                var directory = new DirectoryInfo(path);
                var file = (from f in directory.GetFiles(pattern) orderby f.LastWriteTime descending select f).First();
                logText = System.IO.File.ReadAllText(path + "\\" + file.ToString());
                logText = logText.Replace("Log Entry : ", "<br> Log Entry : ");
            }
            catch 
            {
                logText = "Nenhum arquivo de log encontrado";
            }
            ViewBag.LastLogCreated = logText;

            
            //TIMER//

            //System.Timers.Timer aTimer = new System.Timers.Timer();
            //aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            //aTimer.Interval = 120000;
            //aTimer.Enabled = true;
            
            //END TIMER//
            

            return View();
        }

        void PrimeiraIntegracao()
        {
            string results = "";
            //results += new UMOV.BLL.AgentBLL().CargaInicial();
            results += new UMOV.BLL.ScheduleBLL().CargaInicial();            
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
       {
            //Sincronizar();
        }


        public ActionResult Sincronizar()
        {
            UMOV.BLL.ScheduleBLL scheduleBLL = new ScheduleBLL();
            scheduleBLL.VerificarEIntegrarNovosChamados();
            scheduleBLL.VerificarEAtualizarVisitas();

            return RedirectToAction("Index");
        }
    }
}
