﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HipFat.POCO;

namespace HipFat.DAL
{
    public class ChamadoDAL 
    {
        public ConnectionMySQLDAL oConn = new ConnectionMySQLDAL();
        public string mensagemErro { get; set; }

        public IDataReader ConsultarTodos()
        {
            StringBuilder query = new StringBuilder("SELECT * FROM " +
                "fatchm");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        public IDataReader ConsultarUltimo()
        {
            StringBuilder query = new StringBuilder("Select * FROM fatchm WHERE chmnumero IN (SELECT MAX(chmnumero) FROM fatchm)");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        public IDataReader ConsultarPorID(string id)
        {
            StringBuilder query = new StringBuilder("Select * FROM fatchm WHERE chmnumero = " + id);


            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        internal IDataReader ConsultarNaoIntegrados(string ultimoCodigoUMOV)
        {
            StringBuilder query = new StringBuilder("Select * from fatchm where chmfechado  = 'N' and chmnivel = 3 and chmcodnch <> 1  and chmnumero > " + ultimoCodigoUMOV);

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        public IDataReader ConsultarTodosIntegracaoInicial()
        {
            StringBuilder query = new StringBuilder("Select * from fatchm where chmfechado  = 'N' and chmnivel = 3 and chmcodnch <> 1  and chmdtabert = curdate() and chmnumero in (301683,301684) ");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        public string Atualizar(Chamado chamado)
        {
            string message;
            StringBuilder query = new StringBuilder();
            query.AppendFormat("UPDATE fatchm SET chmnometec = \"{0}\", chmcodtec = 0 WHERE chmnumero = {1} AND chmnivel = 3", chamado.NomeTecnico, chamado.Numero);

            oConn.ExecuteNonQuery(null, query.ToString());

            if (!oConn.CompleteCommand)
                message = "Erro: " + oConn.ErrorMessage + "\n. Durante tentativa de atualizar chamado: " + chamado.Numero.ToString();
            else message = "Atualizado chamado: " + chamado.Numero.ToString();

            return message;
        }
    }
}
