﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipFat.DAL
{
    public class TecnicoDAL
    {
        public ConnectionMySQLDAL oConn = new ConnectionMySQLDAL();
        public string mensagemErro { get; set; }

        public IDataReader ConsultarTodos()
        {
            StringBuilder query = new StringBuilder("SELECT * FROM vw_fatsen");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }
    }
}
