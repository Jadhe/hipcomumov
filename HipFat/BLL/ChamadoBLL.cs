﻿using HipFat.DAL;
using HipFat.POCO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipFat.BLL
{
    public class ChamadoBLL
    {

        readonly ChamadoDAL chamadoDAL = new ChamadoDAL();

        public List<Chamado> ConsultarChamados()
        {
            var chamados = new List<Chamado>();

            IDataReader reader = chamadoDAL.ConsultarTodos();

            chamados = LerDados(reader);

            return chamados;
        }

        public Chamado ConsultarUltimoChamado()
        {
            IDataReader reader = chamadoDAL.ConsultarUltimo();
            var chamados = new List<Chamado>();

            chamados = LerDados(reader);

            return chamados.First();
        }

        public Chamado ConsultarPorID(string id)
        {
            IDataReader reader = chamadoDAL.ConsultarPorID(id);
            var chamados = new List<Chamado>();

            chamados = LerDados(reader);

            return chamados.First();
        }

        public List<Chamado> ConsultarNaoIntegrados(string ultimoCodigoUMOV)
        {
            var chamados = new List<Chamado>();

            IDataReader reader = chamadoDAL.ConsultarNaoIntegrados(ultimoCodigoUMOV);

            chamados = LerDados(reader);

            return chamados;
        }


        private List<Chamado> LerDados(IDataReader reader)
        {
            var chamados = new List<Chamado>();
            if (reader != null)
            {
                while (reader.Read())
                {
                    TimeSpan hour;
                    TimeSpan.TryParse(reader["chmhrabert"].ToString(), out hour);
                    Chamado chamado = new Chamado();
                    chamado.Numero = reader["chmnumero"].ToString();
                    chamado.CodigoCliente = Convert.ToInt32(reader["chmcodcli"].ToString());
                    chamado.NomeTecnico = reader["chmnometec"].ToString();
                    chamado.DataAbertura = (DateTime)reader["chmdtabert"];
                    chamado.HoraAbertura = hour;
                    chamado.AbertoPor = reader["chmabertpor"].ToString();
                    chamado.Motivo = reader["chmmotivo"].ToString();
                    chamado.Codnch = reader["chmcodnch"].ToString();
                    chamado.Prioridade = Convert.ToInt32(reader["chmprior"].ToString());
                    chamado.Fechado = reader["chmfechado"].ToString() == "S" ? true : false;
                    chamado.Nivel = StringToNullableInt(reader["chmnivel"].ToString());

                    chamados.Add(chamado);
                }
            }

            return chamados;
        }

        public List<Chamado> ConsultarTodosParaIntegracaoInicial()
        {
            IDataReader reader = chamadoDAL.ConsultarTodosIntegracaoInicial();
            var chamados = new List<Chamado>();

            chamados = LerDados(reader);

            return chamados;            
        }

        public static int? StringToNullableInt(string strNum)
        {
            int outInt;
            return int.TryParse(strNum, out outInt) ? outInt : (int?)null;
        }

        public string Atualizar(Chamado chamado)
        {
            string resultados;
            resultados = chamadoDAL.Atualizar(chamado);

            return resultados;
        }
    }
}
