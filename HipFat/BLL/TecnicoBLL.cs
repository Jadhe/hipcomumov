﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HipFat.POCO;
using System.Data;
using HipFat.DAL;

namespace HipFat.BLL
{
    public class TecnicoBLL
    {
        TecnicoDAL tecnicoDAL = new TecnicoDAL();

        public List<Tecnico> ConsultarTodos()
        {
            var tecnicos = new List<Tecnico>();

            IDataReader reader = tecnicoDAL.ConsultarTodos();

            while (reader.Read())
            {
                Tecnico tecnico = new Tecnico();
                tecnico.Nome = reader["sennome"].ToString();

                tecnicos.Add(tecnico);
            }
            
            return tecnicos;
        }
    }
}
