﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipFat.POCO
{
    //Representação da view vw_fatchm do banco Hipfat
    public class Chamado
    {
        public string Numero { get; set; }
        public int CodigoCliente { get; set; }
        public string  NomeTecnico { get; set; }
        public DateTime DataAbertura { get; set; }
        public TimeSpan HoraAbertura { get; set; }
        public string AbertoPor { get; set; }
        public string Motivo { get; set; }
        public string Codnch { get; set; }
        public int Prioridade { get; set; }
        public bool Fechado { get; set; }        
        public int? Nivel { get; set; }
    }
}
