﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using UMOV;
using UMOV.BLL;
using HipFat;
using System.Diagnostics;

namespace IntegradorUMOV
{
    static class Program
    {
        public static EventLog serviceLog = new EventLog();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            string path = LogWriter.LogPath;
            string eventSourceName = "IntegradorUMOVSource";
            string logName = "IntegradorUMOV";
            
            if (!EventLog.SourceExists(eventSourceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(eventSourceName, logName);
                return;
            }

            serviceLog.Source = eventSourceName;
            serviceLog.Log = logName;

            serviceLog.WriteEntry("Inicializando Integrador Hipfat UMOV. Diretório do arquivo de Log: "+ path);

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ServicoIntegradorUMOV()
            };
            ServiceBase.Run(ServicesToRun);
        }
        
        public static string IniciarIntegração()
        {
            Program.serviceLog.WriteEntry("IniciarIntegracao?");
            ScheduleBLL scheduleBLL = new ScheduleBLL();
            Program.serviceLog.WriteEntry("Criou Schedule?");
            string resultado = scheduleBLL.VerificarEIntegrarNovosChamados();
            Program.serviceLog.WriteEntry("Verificou?");
            Program.serviceLog.WriteEntry(resultado);
            //scheduleBLL.VerificarEAtualizarVisitas();
            return resultado;
        }
    }
}
