﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using UMOV;
using UMOV.BLL;
using HipFat;


namespace IntegradorUMOV
{
    public partial class ServicoIntegradorUMOV : ServiceBase
    {
        public ServicoIntegradorUMOV()
        {
            InitializeComponent();
            LogWriter.Write("IntegradorUMOV inicializado.");            
        }

        protected override void OnStart(string[] args)
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 120000; //3600000; // 1 hora em milisegundos
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            Program.serviceLog.WriteEntry("Integrador Hipfat UMOV inicializado.");

            string resultadoi = new UMOV.BLL.ScheduleBLL().CargaInicial();
        }

        protected override void OnStop()
        {
            Program.serviceLog.WriteEntry("Integrador Hipfat UMOV pausado.");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Program.serviceLog.WriteEntry("Timer ativado, integração inicializada");
            LogWriter.Write("Iniciando processo de integracao");
            Program.serviceLog.WriteEntry("TESTE");
            string resultado = Program.IniciarIntegração();
            Program.serviceLog.WriteEntry("Importação finalizada. Resultado: " + resultado);
            LogWriter.Write(resultado);            
        }
    }
}
