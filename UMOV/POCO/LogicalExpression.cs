﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO
{
    public class LogicalExpression
    {
        public int id { get; set; }
        public int objectType { get; set; }
        public int moment { get; set; }
        public int objectCode { get; set; }
        public string expressionsForPrint { get; set; }
        public string falseResultMode { get; set; }
        public string validationMessage { get; set; }
        public string emitBeep { get; set; }
        public string executeExpressionVisibleField { get; set; }
        public int ordination { get; set; }
        public string documentation { get; set; }
    }
}
