﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;

namespace UMOV.POCO
{
    public class Schedule
    {
        //Atributos propositalmente com a inicial minuscula para corresponder ao modelo da API; ver Utils.MontarXML que utiliza o nome do atributo para montar os dados
        public bool? active { get; set; }
        public string Image { get; set; }
        public int activitiesOrigin { get; set; }
        public Agent agent { get; set; }
        public int? team { get; set; }
        public int? teamExecution { get; set; }
        public string alternativeIdentifier { get; set; }
        public DateTime date { get; set; }
        public TimeSpan hour { get; set; }
        public DateTime? executionForecastEndDate { get; set; }
        public DateTime? executionForecastEndTime { get; set; }
        public DateTime? waitTimeOnField { get; set; }
        public int? id { get; set; }
        public string observation { get; set; }
        public int? origin { get; set; }
        public ServiceLocal serviceLocal { get; set; }
        public int? situation { get; set; }
        public List<string> activityRelationship { get; set; }
        public int? priority { get; set; }
        public DateTime? toleranceBeforeStart { get; set; }
        public DateTime? toleranceAfterStart { get; set; }
        public DateTime? toleranceBeforeEnd { get; set; }
        public DateTime? toleranceAfterEnd { get; set; }
        public DateTime? toleranceBlockBefore { get; set; }
        public DateTime? toleranceBlockAfter { get; set; }
        public bool? recreateTaskOnPda { get; set; }
        public bool? validateDateTimeExecution { get; set; }
        public List<string> customFields { get; set; }
        
        public Schedule()
        {

        }
            
    }
}
