﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO
{
    public class SectionField
    {
        public int id { get; set; }
        public string alternativeIdentifier { get; set; }
        public int mobilePageNumber { get; set; }
        public int order { get; set; }
        public bool nullable { get; set; }
        public string description { get; set; }
        public bool updatable { get; set; }
        public bool visible { get; set; }
        public string tip { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public int  DECIMAL { get; set; }
        public int active { get; set; }
        public char listType { get; set; }
        public string trueValue { get; set; }
        public string falseValue { get; set; }
        public char sourceValue { get; set; }
        public bool showValueInMobile { get; set; }
        //public List<CustomFieldReference> customFieldReference { get; set; }
        public bool showDataCollected { get; set; }
        public char subType { get; set; }
        public bool presistent { get; set; }
        public char   urlViewType { get; set; }
        //public List<CustomEntity> customEntity { get; set; }
        public string customEntityFieldInternalValue { get; set; }
        public string customEntityFieldExternalValue { get; set; }
        public bool locked { get; set; }
        public string documentation { get; set; }
        public List<MathExpression> mathExpressions { get; set; }
        public List<LogicalExpression> logicalExpressions { get; set; }
    }
}
