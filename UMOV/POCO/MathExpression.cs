﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO
{
    public class MathExpression
    {
        public int id { get; set; }
        public int objectType { get; set; }
        public int objectCode { get; set; }
        public int moment { get; set; }
        public string expressionForPrint { get; set; }
        public string convertEmptyValuesToZero { get; set; }
        public int ordination { get; set; }
        public string documentation { get; set; }
        public int sectionField { get; set; }
    }
}
