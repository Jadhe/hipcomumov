﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMOV.POCO;

namespace UMOV.POCO
{
    public class Activity
    {
        public string description { get; set; }
        public int id { get; set; }
        public List<Section> sections { get; set; }
        public string alternativeIdentifier { get; set; }
        public int displayOrder { get; set; }
        public bool active { get; set; }
        public char comunicationType { get; set; }
        public char executionType { get; set; }
        public bool loopExecution { get; set; }
        public bool showSessionWebForm { get; set; }
        public char confirmClose { get; set; }
        public bool acceleratedExecution { get; set; }
        public bool captureGPS { get; set; }
        public bool confirmWithoutGPS { get; set; }
        public bool GPSIsMandatory { get; set; }
        public bool locked { get; set; }
        public string documentation { get; set; }
        public List<MathExpression> mathExpressions { get; set; }
        public List<LogicalExpression> logicalExpressions { get; set; }
    }
}
