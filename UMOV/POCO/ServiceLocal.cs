﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO
{
    public class ServiceLocal
    {
        //Atributos propositalmente com a inicial minuscula para corresponder ao modelo da API; ver Utils.MontarXML que utiliza o nome do atributo para montar os dados
        public bool? active { get; set; }
        public string image { get; set; }
        public string corporateName { get; set; }
        public string alternativeIdentifier { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string description { get; set; }
        public string geoCoordinate { get; set; }
        public string geoCoordinatePrecision { get; set; }
        public int? id { get; set; }
        public string street { get; set; }
        public string zipCode { get; set; }
        public string streetType { get; set; }
        public int? streetNumber { get; set; }
        public string streetComplement { get; set; }
        public int? cellPhoneIdd { get; set; }
        public int? cellPhoneStd { get; set; }
        public int? cellPhoneNumber { get; set; }
        public int? phoneIdd { get; set; }
        public int? phoneStd { get; set; }
        public int? phoneNumber { get; set; }
        public string email { get; set; }
        public string cityNeighborhood { get; set; }
        public string observation { get; set; }
        public char? accountable { get; set; }
        public List<string> itemsRelationship { get; set; }
        public char? exporStatus { get; set; }
        public bool? pendingExport { get; set; }
        public List<string> customFields { get; set; }
    }
}
