﻿namespace UMOV.POCO
{
    public class AgentType
    {
        public bool? active { get; set; }
        public string alternativeIdentifier { get; set; }
        public string description { get; set; }
        public int? id { get; set; }        
    }
}