﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO

{
    public class Agent
    {
        
        public bool? active { get; set; }
        public string image { get; set; }
        public string alternativeIdentifier { get; set; }
        public AgentType agentType { get; set; }
        public string login { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public int? id { get; set; }
        public string email { get; set; }
        public bool? centerwebUser { get; set; }
        public bool? mobileUser { get; set; }
        public char? centerwebUserRole { get; set; }
        public string accessRole { get; set; }
        public bool? inputWebAsAnotherUser { get; set; }
        public string observation { get; set; }
        public string city { get; set; }
        public string neighborhood { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string street { get; set; }
        public string streetType { get; set; }
        public string zipCode { get; set; }
        public int? streetNumber { get; set; }
        public string streetComplement { get; set; }
        public int? cellphoneIdd { get; set; }
        public int? cellphoneStd { get; set; }
        public int? cellphone { get; set; }
        public int? phoneIdd { get; set; }
        public int? phoneStd { get; set; }
        public int? phone { get; set; }
        public char? validateClient { get; set; }
        public bool? changePassword { get; set; }
        public DateTime? lastSynchronismDate { get; set; }
        public DateTime? lastSynchronismTime { get; set; }
        public bool? lockLoginInChangeImei { get; set; }
        public string imeiLastSynchronism { get; set; }
        public bool? memorizePasswordMobile { get; set; }
        public string geoLocation { get; set; }
        public List<string> customFields { get; set; }

        public bool JaExisteNoUmov()
        {
            string result = new AcessaAPI().GetResults("agent", "?login=" + this.login.ToString());
            int encontrou = result.IndexOf("<entry id=");
            if (encontrou != -1)
                return true;
            else return false;
        }
        
    }
}
