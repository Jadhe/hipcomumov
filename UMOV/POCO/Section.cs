﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.POCO
{
    public class Section
    {
        public int id { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public List<SectionField> sectionFields { get; set; }
        public int alternativeIdentifier { get; set; }
        public char mandatory { get; set; }
        public char useItem { get; set; }
        public bool findItemsByIdentifier { get; set; }
        public char itemFillMode { get; set; }
        public char groupingItensTypeOnMobile { get; set; }
        public char activityHistoryReportType { get; set; }
        public char displayItemsInMobile { get; set; }
        public bool seeItemsCollectedAutomatically { get; set; }
        public bool quizMode { get; set; }
        public int numberFieldsQuiz { get; set; }
        public bool locked { get; set; }
        public bool markCompleteGroup { get; set; }
        public string documentation { get; set; }
        public List<MathExpression> mathExpressions { get; set; }
        public List<LogicalExpression> logicalExpressions { get; set; }

    }
}
