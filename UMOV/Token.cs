﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace UMOV
{
    internal static class Token
    {

        private static string MontarXML()
        {
            string login = "jadhe";
            string pass = "40837254";
            string domain = "hipfatmobile";

            StringBuilder XML = new StringBuilder();
            XML.Append("data=");
            XML.Append("<apiToken>");
            XML.Append("<login>"+login+"</login>");
            XML.Append("<password>"+pass+"</password>");
            XML.Append("<domain>"+domain+"</domain>");
            XML.Append("</apiToken>");

            return XML.ToString();
        }

        public static string BuscarToken()
        {
            string URL = "https://api.umov.me/CenterWeb/api/token.xml";

            string responseString = "";
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                string xml = MontarXML();
                
                StringContent sc = new StringContent(xml);

                sc.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded");

                var response = client.PostAsync(URL, sc).Result;



                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    responseString = responseContent.ReadAsStringAsync().Result;
                    int start = responseString.IndexOf("<message>") + "<message>".Length;
                    int end = responseString.IndexOf("</message>");
                    int length = end - start;

                    responseString = responseString.Substring(start, length);
                }
                else responseString = response.RequestMessage.ToString();
            }

            return responseString;
        }
    }
}
