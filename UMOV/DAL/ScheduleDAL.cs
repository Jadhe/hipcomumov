﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.DAL
{
    public class ScheduleDAL
    {
        public ConnectionPostgreDAL oConn = new ConnectionPostgreDAL();
        public string mensagemErro { get; set; }

        public IDataReader ConsultarTodos()
        {
            StringBuilder query = new StringBuilder("SELECT * FROM " +
                "task");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }

        /// <summary>
        /// Retorna os registros de schedule do banco do UMOV que tenham data de update maior que a data de criacao e cuja modificacao tenha sido feita no site
        /// </summary>
        /// <returns></returns>
        public IDataReader ConsultarTodosComAlteracao()
        {
            StringBuilder query = new StringBuilder("SELECT T.*, A.age_integrationid, A.age_name, A.age_login "+
                                                    "FROM task T INNER JOIN agent A ON T.age_id = A.age_id " +
                                                    "WHERE tsk_modulelastupdate = 'Center' " +
                                                    "AND tsk_datetimelastupdate > tsk_datetimeinsert");

            oConn.ExecuteReader(null, query.ToString());
            IDataReader reader = oConn.Reader;
            if (reader == null)
                mensagemErro = oConn.ErrorNumber.ToString() + " - " + oConn.ErrorMessage;

            return reader;
        }
    }
}
