﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.BLL
{
    public class AgentTypeBLL
    {
        public static int? ConsultarCodigoTecnico()
        {
            string codigo="";
            string busca = "Técnico";
            string resposta = new AcessaAPI().GetResults("agentType", "?description="+ busca);

            string tag = "id=";
            int start = resposta.IndexOf(tag) + tag.Length;
            int end = resposta.IndexOf(" link=");
            if (end != -1)
            {
                end = end - start;
                codigo = resposta.Substring(start, end);
            }
            return Int32.Parse(codigo);
        }
    }
}
