﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMOV.POCO;
using HipFat.BLL;
using HipFat.POCO;
using System.Data;
using UMOV.DAL;

namespace UMOV.BLL
{
    public class ScheduleBLL
    {
        public ScheduleDAL DAL = new ScheduleDAL();
        public Schedule MontarVisita(int codigoChamadoHipfat)
        {
            Schedule visita = new Schedule();

            HipFat.POCO.Chamado chamadoHipFat = new ChamadoBLL().ConsultarPorID(codigoChamadoHipfat.ToString());
            
            string codigoUMOV = ClienteBLL.ConsultarCodigoUMOV(chamadoHipFat.CodigoCliente.ToString());

            visita.date = chamadoHipFat.DataAbertura;
            visita.hour = chamadoHipFat.HoraAbertura;
            visita.serviceLocal = new ServiceLocal();
            visita.serviceLocal.id = Convert.ToInt32(codigoUMOV);
            visita.activitiesOrigin = 3;
            visita.observation = chamadoHipFat.Motivo;
            visita.priority = chamadoHipFat.Prioridade;

            string xml = Utils.TransfomarXML(visita);

            return visita;
        }

        /// <summary>
        /// Metodo responsavel por realizar a primeira carga do sistema de integração antes do seriço ser ativado para rodar em intervalos.
        /// Deve importar a carga inicial de dados do hipfat para o UMOV
        /// </summary>
        public string CargaInicial()
        {
            string responseString = "";
            List<HipFat.POCO.Chamado> chamadosIniciaisHipFat = new ChamadoBLL().ConsultarTodosParaIntegracaoInicial();
            
            foreach(Chamado chamado in chamadosIniciaisHipFat)
            {
                Schedule visita = new Schedule();

                string codigoUMOV = ClienteBLL.ConsultarCodigoUMOV(chamado.CodigoCliente.ToString());
                visita.date = chamado.DataAbertura;
                visita.hour = chamado.HoraAbertura;
                visita.serviceLocal = new ServiceLocal();
                visita.serviceLocal.id = Convert.ToInt32(codigoUMOV);
                visita.activitiesOrigin = 3;
                visita.observation = chamado.Motivo;
                visita.priority = chamado.Prioridade;
                visita.alternativeIdentifier = chamado.Numero;

                string xml = Utils.TransfomarXML(visita);

                responseString += new AcessaAPI().PostData("schedule", xml);

            }

            return responseString;
        }

        /// <summary>
        /// Metodo responsavel por realizar a primeira carga do sistema de integração antes do seriço ser ativado para rodar em intervalos.
        /// Deve importar a carga inicial de dados do hipfat para o UMOV
        /// </summary>
        public string VerificarEIntegrarNovosChamados()
        {
            string responseString = "";

            string ultimoCodigoUMOV = RecuperarCodigoUltimoChamadoIntegrado();
            HipFat.POCO.Chamado ultimoChamadoHipfat = new ChamadoBLL().ConsultarUltimoChamado();
            if (ultimoCodigoUMOV != ultimoChamadoHipfat.Numero)
            {

                List<HipFat.POCO.Chamado> chamadosIniciaisHipFat = new ChamadoBLL().ConsultarNaoIntegrados(ultimoCodigoUMOV);
                if (chamadosIniciaisHipFat.Count > 0)
                {
                    foreach (Chamado chamado in chamadosIniciaisHipFat)
                    {
                        Schedule visita = new Schedule();
                        string observation, xml;
                        string codigoUMOV = ClienteBLL.ConsultarCodigoUMOV(chamado.CodigoCliente.ToString());
                        if(codigoUMOV == "-1")
                        {
                            LogWriter.Write("Cliente id = " + chamado.CodigoCliente.ToString() + " não cadastrado no UMOV.");
                            continue;
                        }

                        visita.date = chamado.DataAbertura;
                        if (visita.date < DateTime.Now.Date)
                        {
                            LogWriter.Write("Data de abertura do chamado: " + chamado.Numero + " menor que a de hoje. " + visita.date.ToString("yyyy-MM-dd"));
                            continue;
                        }
                        visita.hour = chamado.HoraAbertura;
                        visita.serviceLocal = new ServiceLocal();
                        visita.serviceLocal.id = Convert.ToInt32(codigoUMOV);
                        visita.activitiesOrigin = 3;
                        
                        if (chamado.Motivo.Count() > 500)
                        {
                            LogWriter.Write("Data de abertura do chamado: " + chamado.Numero + " menor que a de hoje. " + visita.date.ToString("yyyy-MM-dd"));
                            observation = chamado.Motivo.Substring(0, 500);
                        }
                        else observation = chamado.Motivo;

                        visita.observation = observation;
                        visita.priority = chamado.Prioridade;
                        visita.alternativeIdentifier = chamado.Numero;

                        xml = Utils.TransfomarXML(visita);

                        responseString += new AcessaAPI().PostData("schedule", xml);
                    }
                }
            }

            return responseString;
        }

        /// <summary>
        /// Metodo responsavel por realizar a verificação do ultimo chamado inserido no UMOV conforme os dados existentes no HipFat
        /// </summary>
        /// <returns>AlternativeIdentifier/UMOV - nr_chamdo/HipFat</returns>
        public string RecuperarCodigoUltimoChamadoIntegrado()
        {
            string responseString = "";

            //Busca no UMOV qual é o idUMOV do ultimo schedule importado
            responseString += new AcessaAPI().GetResults("schedule", "");
            int start = responseString.IndexOf("<entry id=") + "<entry id='".Length;
            int final = responseString.IndexOf("link=") - 2;
            int quantidade = final - start;

            string idUMOVUltimoChamado = responseString.Substring(start, quantidade);

            //Busca atraves da api umov as informações desse ultimo chamado importado para encontrar o idHipfat (alternativeIdentifier)
            responseString = new AcessaAPI().GetResults("schedule", idUMOVUltimoChamado);
            //retiramos as informações do cliente pois ele também possui um alternativeIdentifier e isso pode gerar problemas ao buscar essa informação trazendo o idCliente ao inves do idChamado
            start = responseString.IndexOf("<serviceLocal>");
            final = responseString.IndexOf("</serviceLocal>") + "</serviceLocal>".Length;
            quantidade = final - start;

            string apenasChamado = responseString.Remove(start, quantidade);

            string tag = "<agent>";
            if (apenasChamado.IndexOf(tag) > -1)
            {
                start = apenasChamado.IndexOf("<agent>") + tag.Length;
                final = apenasChamado.IndexOf("</agent>");
                quantidade = final - start;

                apenasChamado = apenasChamado.Remove(start, quantidade); //retirando informações de técnico
            }

            tag = "<alternativeIdentifier>";
            start = apenasChamado.IndexOf("<alternativeIdentifier>") + tag.Length;
            final = apenasChamado.IndexOf("</alternativeIdentifier>");
            quantidade = final - start;

            string codigo = apenasChamado.Substring(start, quantidade);
            
            return codigo;            
        }

        private List<Schedule> LerDados(IDataReader reader)
        {
            var schedules = new List<Schedule>();

            while (reader.Read())
            {
                //TimeSpan hour;
                //TimeSpan.TryParse(reader["chmhrabert"].ToString(), out hour);

                Schedule schedule = new Schedule();
                schedule.id = Convert.ToInt32(reader["tsk_id"].ToString());
                schedule.alternativeIdentifier = reader["tsk_integrationid"].ToString();
                schedule.agent = new Agent();
                schedule.agent.id = Convert.ToInt32(reader["age_id"].ToString());
                schedule.agent.alternativeIdentifier = reader["age_integrationid"].ToString();
                schedule.agent.name = reader["age_name"].ToString();

                schedules.Add(schedule);
            }

            return schedules;
        }

        /// <summary>
        /// Verifica na base e atualiza todos os dados modificados.
        /// </summary>
        public void VerificarEAtualizarVisitas()
        {
            IDataReader reader = DAL.ConsultarTodosComAlteracao();
            var schedules = new List<Schedule>();
            schedules = LerDados(reader);
            ChamadoBLL bll = new ChamadoBLL();

            foreach (var schedule in schedules)
            {
                Chamado chamado = new Chamado();
                chamado.Numero = schedule.alternativeIdentifier;
                chamado.NomeTecnico = schedule.agent.alternativeIdentifier;

                string resultado = bll.Atualizar(chamado);
                LogWriter.Write(resultado);
            }

            return;
        }
    }
}
