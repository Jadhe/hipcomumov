﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HipFat;
using HipFat.POCO;
using HipFat.BLL;
using UMOV.POCO;

namespace UMOV.BLL
{
    public class AgentBLL
    {
        public string CargaInicial()
        {
            string responseString = "";
            List<Tecnico> tecnicosHF = new TecnicoBLL().ConsultarTodos();

            foreach (Tecnico tecnico in tecnicosHF)
            {
                Agent agent = new Agent();
                string nome = Utils.RemoverAcentuacao(tecnico.Nome);

                agent.alternativeIdentifier = nome;
                agent.name = tecnico.Nome;
                nome = nome.Replace(" ", "_");
                agent.login = nome;
                agent.agentType = new AgentType();
                agent.agentType.id = AgentTypeBLL.ConsultarCodigoTecnico(); //Tipo: Técnico
                agent.password = "mudar123=";

                if (agent.JaExisteNoUmov()) continue;

                string xml = Utils.TransfomarXML(agent);
                responseString += new AcessaAPI().PostData("agent", xml);
                LogWriter.Write("Integrado Técnico: " + tecnico.Nome);

            }
            return responseString;
        }
    }
}
