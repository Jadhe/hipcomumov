﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV.BLL
{
    public static class ClienteBLL
    {
        public static string ConsultarCodigoUMOV(string idHipFat)
        {
            string codigo = "-1";
            string codigoCliente = "alternativeIdentifier/" + idHipFat.ToString();
            string resposta = new AcessaAPI().GetResults("serviceLocal", codigoCliente);

            string tag = "<id>";
            int start = resposta.IndexOf("<id>") + tag.Length;
            int end = resposta.IndexOf("</id>");
            if (end != -1)
            {
                end = end - start;
                codigo = resposta.Substring(start, end);
            }
            return codigo;
        }
    }
}
