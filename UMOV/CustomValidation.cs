﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMOV
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class CustomValidation : System.Attribute
    {
            public bool IsRequired { get; set; }
            public decimal FieldLength { get; set; }
            public Type FieldType { get; set; }

            public CustomValidation()
            {
            }       

    }
}
