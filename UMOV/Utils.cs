﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UMOV
{
    public class Utils
    {

        public static string TransfomarXML(object objeto)
        {
            StringBuilder XML = new StringBuilder();
            PropertyInfo[] properties = objeto.GetType().GetProperties();

            int start = objeto.GetType().ToString().LastIndexOfAny(new char[] { '.' }) + 1;
            string tipo = objeto.GetType().ToString().Substring(start).First().ToString().ToLower() + objeto.GetType().ToString().Substring(start+1);
            XML.Append("data=");
            XML.Append("<" + tipo + ">");

            foreach (var propertyInfo in properties)
            {
                object propertyValue = propertyInfo.GetValue(objeto, null);
                if (propertyValue != null && propertyValue.ToString() != "")
                {
                    if (propertyInfo.GetMethod.ReturnType == (typeof(DateTime)))
                    {
                        XML.Append("<" + propertyInfo.Name + ">");
                        DateTime formatData = (DateTime)propertyValue;
                        XML.Append(formatData.ToString("yyyy-MM-dd"));
                        XML.Append("</" + propertyInfo.Name + ">");
                    }
                    else if (propertyInfo.GetMethod.ReturnType == (typeof(TimeSpan)))
                    {
                        XML.Append("<" + propertyInfo.Name + ">");
                        TimeSpan formatData = (TimeSpan)propertyValue;
                        XML.Append(formatData.ToString(@"hh\:mm"));
                        XML.Append("</" + propertyInfo.Name + ">");
                    }
                    else if(propertyInfo.GetMethod.ReturnType == (typeof(string)))
                    {
                        XML.Append("<" + propertyInfo.Name + ">");
                        XML.Append(Convert.ToString(propertyValue));
                        XML.Append("</" + propertyInfo.Name + ">");
                    }
                    else if (propertyInfo.GetMethod.ReturnType.IsClass)
                    {
                        string objetoFilho = TransfomarXML(propertyValue);
                        objetoFilho = objetoFilho.Remove(0, "data=".Length);
                        XML.Append(objetoFilho);
                    }
                    else
                    {
                        XML.Append("<" + propertyInfo.Name + ">");
                        XML.Append(Convert.ToString(propertyValue));
                        XML.Append("</" + propertyInfo.Name + ">");
                    }
                }
            }

            XML.Append("</" + tipo + ">");

            return XML.ToString();
        }

        public static string RemoverAcentuacao(string text)
        {
            return new string(text
                .Normalize(NormalizationForm.FormD)
                .Where(ch => char.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                .ToArray());
        }

    }
}
