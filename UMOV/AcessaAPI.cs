﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UMOV.POCO;

namespace UMOV
{
    public sealed class AcessaAPI
    {
        private static readonly HttpClient client = new HttpClient();
        private static string URL = "";

        /// <summary>
        /// Para consultar um registro ou mais de determinado modulo
        /// </summary>
        /// <param name="modulo"></param>
        /// <param name="keys">Parametros de consulta. Para trazer todos, keys = null; Para consulta enviar keys = "?+parametro=valorAConsultar1&parametro=valorAConsultar2"; Especifico keys = ID</param>
        /// <returns></returns>
        public string GetResults(string modulo, string keys)
        {
            URL = MontarURL(modulo, keys, Action.GET);
            string responseString = "";
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                var response = client.GetAsync(URL).Result;

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    responseString = responseContent.ReadAsStringAsync().Result;
                }
                else responseString = response.RequestMessage.ToString();
            }
            return responseString;
        }

        /// <summary>
        /// Para realizar um post com determinados parametros num modulo especifico
        /// </summary>
        /// <param name="modulo"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public string PostData(string modulo, string keys)
        {
            URL = MontarURL(modulo, keys, Action.POST);
            string responseString = "";
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                StringContent stringContent = new StringContent(keys);
                stringContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded");

                var response = client.PostAsync(URL, stringContent).Result;

                try
                {
                    response.EnsureSuccessStatusCode();

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;
                        responseString = responseContent.ReadAsStringAsync().Result;
                    }
                    else responseString = response.RequestMessage.ToString();
                }
                catch(Exception e)
                {
                    LogWriter.Write("ERRO AO REALIZAR POST: \n" + response.RequestMessage.ToString());
                    responseString = response.RequestMessage.ToString();

                }
            }

            return responseString;
        }            
        
        /// <summary>
        /// Monta e retorna a URL correta para a chamada desejada
        /// </summary>
        /// <param name="modulo"></param>
        /// <param name="keys"></param>
        /// <param name="acao"></param>
        /// <returns></returns>
        private static string MontarURL(string modulo, string keys, Action acao)
        {
            //TESTES
            //string token = "19966ee96f0baf5e9fd3ac21012b7dba1e7dbd"; testedesen
            //string montaURL = "https://testedesen.umov.me/CenterWeb/api/" + token + "/";

            //PADRAO
            string token = Token.BuscarToken();
            
            string montaURL = "https://api.umov.me/CenterWeb/api/" + token + "/";

            if (modulo.Contains(".xml")) modulo = modulo.Substring(0, modulo.IndexOf(".") - 1);
            
            if (acao.Equals(Action.POST))
            {
                montaURL = montaURL + modulo + ".xml";
            }
            else if (acao.Equals(Action.GET))
            {
                if (keys == null || keys == "")
                {
                    montaURL = montaURL + modulo + ".xml";
                }
                else if(keys.Contains("?"))
                {
                    montaURL = montaURL + modulo + ".xml" + keys;
                }
                else
                {
                    montaURL = montaURL + modulo + "/" + keys + ".xml";
                }
            }

            return montaURL;

        }
    }

    enum Action
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    
}
