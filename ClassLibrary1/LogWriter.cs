﻿using System;
using System.IO;
using System.Reflection;

public static class LogWriter
{
    public static string LogPath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory) + "\\Log";

    //public LogWriter(string logMessage)
    //{
    //    LogWrite(logMessage);
    //}
    public static void Write(string logMessage)
    {
        try
        {
            using (StreamWriter w = File.AppendText(LogPath + "\\" + DateTime.Now.Date.ToString("yyyy-MM-dd") + ".txt"))
            {
                Log(logMessage, w);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public static void Log(string logMessage, TextWriter txtWriter)
    {
        try
        {
            txtWriter.Write("\r\nLog Entry : ");
            txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            txtWriter.WriteLine("  :{0}", logMessage);
            txtWriter.WriteLine("-------------------------------");
        }
        catch (Exception ex)
        {
        }
    }
}