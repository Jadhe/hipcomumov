﻿using System;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

public class ConnectionMySQLDAL
{
    #region Variaveis Globais
    private bool _State;
    private string _ErrorMessage;
    private int _ErrorNumber;
    private bool _CompleteCommand;
    private IDataReader _rsData;
    private object _scalReturn;
    private MySqlConnection _connect;
    #endregion

    #region Definicoes
    public IDataReader Reader { get { return _rsData; } }
    public object Scalar { get { return _scalReturn; } }
    public bool State { get { return _State; } }
    public string ErrorMessage { get { return _ErrorMessage; } }
    public int ErrorNumber { get { return _ErrorNumber; } }
    public bool CompleteCommand { get { return _CompleteCommand; } }
    private MySqlConnection getConnection(string connectionString)
    {
        string connString = ConfigurationManager.ConnectionStrings["hipfat_context"].ToString();
        return new MySqlConnection(connString);
    }

    public MySqlConnection openConnection(string connectionString)
    {
        try
        {
            _connect = getConnection(connectionString);
            if (_connect.State != System.Data.ConnectionState.Open)
            {
                _connect.Open();
                _State = true;
            }
        }
        catch (Exception ex)
        {
            _ErrorNumber = ex.GetHashCode();
            _ErrorMessage = ex.Message.ToString();
            _State = false;
        }
        return _connect;
    }

    public void closeConnection()
    {
        try
        {
            if (_connect.State == System.Data.ConnectionState.Open)
            {
                _connect.Close();
                _connect.Dispose();
            }
        }
        catch (Exception ex)
        {
            _ErrorNumber = ex.GetHashCode();
            _ErrorMessage = ex.Message.ToString();
            _State = false;
        }
    }
    #endregion

    #region Metodos
    public void ExecuteReader(string connectionString, string queryString)
    {

        MySqlCommand command = new MySqlCommand(queryString);
        command.CommandType = CommandType.Text;
        ExecuteReader(connectionString, command);

    }

    public void ExecuteReader(string connectionString, IDbCommand command)
    {
        IDataReader reader;

        try
        {
            using (MySqlConnection mysqlConnection = openConnection(connectionString))
            using (command)
            {
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 1200;
                command.Connection = mysqlConnection;

                reader = command.ExecuteReader();
                var dt = new DataTable();
                dt.Load(reader);
                _rsData = dt.CreateDataReader();
            }
            _CompleteCommand = true;
        }
        catch (Exception ex)
        {
            _ErrorNumber = ex.GetHashCode();
            _ErrorMessage = ex.Message.ToString();
            _State = false;
        }
    }

    public void ExecuteScalar(string connectionString, string queryString)
    {
        MySqlCommand command = new MySqlCommand(queryString);
        command.CommandType = CommandType.Text;
        ExecuteScalar(connectionString, command);
    }

    public Object ExecuteScalar(string connectionString, IDbCommand command)
    {
        try
        {
            using (MySqlConnection mysqlConnection = openConnection(connectionString))
            using (command)
            {
                command.CommandType = CommandType.Text;
                command.Connection = mysqlConnection;
                command.CommandTimeout = 1200;
                _scalReturn = command.ExecuteScalar();
            }
            _CompleteCommand = true;
            return _scalReturn;
        }
        catch (Exception ex)
        {
            _ErrorNumber = ex.GetHashCode();
            _ErrorMessage = ex.Message.ToString();
            _State = false;
            return null;
        }
    }

    public void ExecuteNonQuery(string connectionString, string queryString)
    {

        MySqlCommand command = new MySqlCommand(queryString);
        command.CommandType = CommandType.Text;
        ExecuteNonQuery(connectionString, command);
    }

    public void ExecuteNonQuery(string connectionString, IDbCommand command)
    {
        try
        {
            using (MySqlConnection mysqlConnection = openConnection(connectionString))
            using (command)
            {
                command.CommandType = CommandType.Text;
                command.Connection = mysqlConnection;
                command.CommandTimeout = 1200;
                var s = command.ExecuteNonQuery();
                s.ToString();
            }
            _CompleteCommand = true;
        }
        catch (Exception ex)
        {
            _ErrorNumber = ex.GetHashCode();
            _ErrorMessage = ex.Message.ToString();
            _State = false;
        }
    }
    #endregion
}

